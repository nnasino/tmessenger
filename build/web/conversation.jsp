<%-- 
    Document   : conversation
    Created on : Jun 22, 2014, 10:01:41 PM
    Author     : Chigozirim
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="com.torti.functions.Functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="templates/header.jsp"%>
<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">TMessenger</a>
        <ul class="nav">
            <li><a href="index.jsp">Home</a></li>
            <li><a href="view_profile.jsp">Profile</a></li>
            <li class="active"><a href="conversations.jsp?s=0">Messages</a></li>
        </ul>
        <ul class="nav pull-right">
            <li class="pull-right"><a class="pull-right" href="logout.jsp">Log Out</a></li>
        </ul>
    </div>
</div>



<div class="container text-center">
    <%
        //Store the threadId incase we need to post a new reply
        session.setAttribute("currentThreadId", request.getParameter("t"));
        //If the connection to the database is closed reconnect
        if (Functions.conn.isClosed()) {
            Functions.conn = DriverManager.getConnection(Functions.CONNECTION_URL,
                    Functions.USERNAME, Functions.PASSWORD);
        }

        //Query the database for the number of posts in this thread
        Functions.stmt = Functions.conn.createStatement();
        ResultSet set = Functions.stmt.executeQuery(String.format(Functions.noOfPostsQuery,
                request.getParameter("t")));

        //Get the point where we start loading up posts
        int postStart = Integer.parseInt(request.getParameter("s")) * 20;
        session.setAttribute("postStart", postStart);
        set.next();
        int count = set.getInt(1);

        set.close();
        //if stmt is closed create a  new one
        if (Functions.stmt.isClosed()) {
            Functions.stmt = Functions.conn.createStatement();
        }

        //Query the database for all the posts in this thread
        set = Functions.stmt.executeQuery(String.format("SELECT `Title`, `AuthorId`, `ReceiverId` FROM `threads` WHERE `ThreadId` = %s",
                request.getParameter("t")));
        set.next();
        HashMap<String, String> threadData = new HashMap();
        threadData.put("title", set.getString(1));
        threadData.put(set.getString(2), Functions.getUserName(set.getString(2)));
        threadData.put(set.getString(3), Functions.getUserName(set.getString(3)));
        Functions.stmt = Functions.conn.createStatement();
        String sqlStatement = String.format(Functions.fetchPostsQuery,
                request.getParameter("t"), postStart);
        ResultSet posts = Functions.stmt.executeQuery(sqlStatement);
        int i = 0; //used to keep count of the posts that have been rendered

        while (posts.next()) {
            i++;
            if (i == 1) {%>  

        <div class="post-header">
            <h3><%=threadData.get("title")%></h3>
                <span style="color: #a4a4a4;font-size: 12px;" class="muted">Started by <%=(threadData.get(posts.getString(2)).equals(session.getAttribute("username")))
                        ? "You" : threadData.get(posts.getString(2))%> on 
                <%= Functions.dateFormatter.format(posts.getDate(5))%>, <%= Functions.timeFormatter.format(posts.getTime(5))%></span>
        </div>
  
    <%}%>
    <div id="<%=i%>" class="post-header"><%=(threadData.get(posts.getString(2)).equals(session.getAttribute("username")))
            ? "<span><a href=\"view_profile.jsp?user=" + threadData.get(posts.getString(2)) + "\">You</a></span>"
            : "<span><a href=\"view_profile.jsp?user=" + threadData.get(posts.getString(2)) + "\">" + threadData.get(posts.getString(2)) + "</a></span>"%>
        <span style="float: right"><a href="#<%=i%>">#<small><%=i%></small></a></small>
    </div>
    <div class="post">
        <span class="time"><%= Functions.timeFormatter.format(posts.getTime(5))%><%= Functions.dateFormatter.format(posts.getDate(5))%></span>
        <br/>
        <%=posts.getString(3)%>
        <hr/>
        <br/>

    </div>

    <% }%>
    <div class="inline">
        <div class="text-right"><button class="btn"><a href="post_form.jsp">Create new reply</a></button></div>
        <!--Pagination-->
        <div class="pagination pagination-right">
            <div class="pagination">
                <ul>
                    <!--if this is the first page, disable the previous pagination button else set the previous button-->
                    <li class="<%= (request.getParameter("s").equals("0")) ? "disabled" : "enabled"%>">
                        <%=(postStart == 0) ? "<a>&laquo;</a>"
                                : "<a href=\"conversation.jsp?t=" + request.getParameter("t") + "&s="
                                + ((postStart - 20) / 20) + "\">&laquo;</a>"%></li>

                    <% int j = 0;
                    //Do the pagination, 1, 2, 3 ....
                while (j < count) {%>
                    <li <%=(postStart == (j)) ? "class=\"active\"><a>" + (j / 20 + 1) + "</a>" : "><a href=\"conversation.jsp?t="
                    + request.getParameter("t") + "&s=" + (j / 20) + "\">" + (j / 20 + 1) + "</a>"%></li>       
                        <%       j += 20;
                    }%>
                    <li class="<%= (count - postStart <= 20) ? "disabled" : "enabled"%>">
                        <%=(count - postStart <= 20) ? "<a>&raquo;</a>"
                        : "<a href=\"conversation.jsp?t=" + request.getParameter("t") + "&s=" + ((postStart + 20) / 20) + "\">&raquo;</a>"%></li>
                </ul>
            </div>
        </div>

    </div>
                <%@include file="templates/footer.jsp"%>