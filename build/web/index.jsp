<%-- 
    Document   : index
    Created on : Jun 10, 2014, 8:59:37 PM
    Author     : Chigozirim
--%>
<%@page import="java.sql.DriverManager"%>
<%@page import="com.torti.functions.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% if (session.getAttribute("username") == null) {
        request.setAttribute("ErrorMsg", "");
        Functions.setErrorMsg("");
        session.setAttribute("title", "Log In");
        RequestDispatcher req = request.getRequestDispatcher("login.jsp");
        req.forward(request, response);
    } else {
        response.sendRedirect("conversations.jsp?s=0");
    }%>

