<%-- 
    Document   : createmessage_form
    Created on : Jun 24, 2014, 2:38:22 PM
    Author     : Chigozirim
--%>

<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="templates/header.jsp"%>

<div class='container text-center'>
    <p class="text-error"><%=com.torti.functions.Functions.PostThreadErrorMsg%></p>
    <p class="text-center">Create new Reply</p>
    <form method="post" action="createmessage.jsp">
        <input type='text'  name="title" id="title" placeholder="Title/Subject" /><br/>
        <input type='text' name='receiver' placeholder="Receiver's Username"/><br/>
        <textarea rows = "9" cols="20" name="posttext" placeholder="Enter your message text...." autofocus required></textarea><br/>
        <button type="submit" class="btn btn-primary">Create Reply</button> 
    </form>
</div>
    <div class="text-center"><a href="index.jsp">Back</a></div>
<%@include file="templates/footer.jsp"%>