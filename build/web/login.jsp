<%-- 
    Document   : login
    Created on : Jun 12, 2014, 8:45:27 AM
    Author     : Chigozirim
--%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@page import="com.torti.functions.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%! String err = "";%>
<%
    //Only process if the form was submitted
    if (request.getMethod().equals("POST")) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
      
        try {
            Functions.loginResult = Functions.login(username, password);
        } catch (SQLException sql) {
            err = "Invalid Username/Password";
            Functions.setErrorMsg(err);
            session.setAttribute("title", "Log In");
            request.getRequestDispatcher("login_form.jsp").forward(request, response);
        }
        session.setAttribute("debug", String.format("username %s password %s", username, password));
        switch (Functions.loginResult) {
            case Functions.PASSWORD_INCORRECT:
                err = "Incorrect Password";
                Functions.setErrorMsg(err);
                session.setAttribute("title", "Log In");
                request.getRequestDispatcher("login_form.jsp").forward(request, response);
                break;
            case Functions.USER_NOT_FOUND:
                err = "Invalid Username/Password";
                request.setAttribute("ErrorMsg", err);
                session.setAttribute("title", "Log In");
                request.getRequestDispatcher("login_form.jsp").forward(request, response);
                break;
            default:
                session.setAttribute("username", username);
                session.setAttribute("userId", Functions.loginResult);
                session.setAttribute("threadStart", 0);
                session.setAttribute("title", "Conversations");
                Functions.setErrorMsg("");
                response.sendRedirect("conversations.jsp?s=0");

        }
    } else {
        if (session.getAttribute("username") == null) {
            if (request.getAttribute("isLogout") == null) {
                Functions.setErrorMsg("");
                request.getRequestDispatcher("login_form.jsp").forward(request, response);
            } else {
                Functions.setErrorMsg(err);
                request.getRequestDispatcher("login_form.jsp").forward(request, response);
            }
            request.setAttribute("title", "Log In");
        } else {
            Functions.setErrorMsg("");
            request.getRequestDispatcher("conversations.jsp").forward(request, response);
        }
    }%>
