<%-- 
    Document   : createmessage
    Created on : Jun 24, 2014, 2:38:49 PM
    Author     : Chigozirim
--%>

<%@page import="java.util.Calendar"%>
<%@page import="com.torti.functions.Functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int receiverId = Functions.getUserId(request.getParameter("receiver"));
    Functions.PostThreadErrorMsg = Integer.toString(receiverId);
    if(receiverId ==-1){
        Functions.PostThreadErrorMsg = "The specified receiver username does not exist";
        request.getRequestDispatcher("createmessage_form.jsp").forward(request, response);
    }else{
        Functions.PostThreadErrorMsg = "user was found";
        if(Functions.addThread(Functions.getUserId(session.getAttribute("username").toString()), receiverId, 
request.getParameter("title"), request.getParameter("posttext"), Calendar.getInstance().getTime())){
            Functions.postThreadMsg = "Your message was successfully posted!";
            response.sendRedirect("conversations.jsp?s=0");
        }   
    
    }
    
    %>