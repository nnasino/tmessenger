<%-- 
    Document   : register
    Created on : Jun 16, 2014, 10:04:12 AM
    Author     : Chigozirim
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="com.torti.functions.Functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    if (request.getMethod().equals("POST")) {
        String userName, email, firstName, lastName, password, sex, location;
        //Fetch all inputs to the form
        userName = request.getParameter("uname");
        email = request.getParameter("email");
        firstName = request.getParameter("fname");
        lastName = request.getParameter("lname");
        password = request.getParameter("pword1");
        sex = request.getParameter("sex");
        location = request.getParameter("loc");
        int result = 0;
        //Make sure both passwords match
        if (request.getParameter("pword1").equals(request.getParameter("pword2"))) {
            try {
                result = Functions.addUser(userName.toLowerCase(), email, firstName, lastName, password, sex, location);
                //If the user was successfully added
                if (result == 1) {
                    session.setAttribute("username", userName);
                    session.setAttribute("userId", Functions.login(userName, password));
                    response.sendRedirect("conversations.jsp?s=0");
                } else {
                    Functions.setRegisterErrorMsg("There was an error registering you, please try again!");
                    response.sendRedirect("register_form.jsp");
                }
            } catch (SQLException sqlexception) {
                if (sqlexception.getErrorCode() == 1062) {
                    if (sqlexception.getMessage().contains(email)) {
                        Functions.setRegisterErrorMsg("The specified email is already associated with an existing account");
                        response.sendRedirect("register_form.jsp");
                    } else if (sqlexception.getMessage().contains(userName)) {
                        Functions.setRegisterErrorMsg("The username is already taken");
                        response.sendRedirect("register_form.jsp");
                    }
                }
            }
        } else {
            Functions.setRegisterErrorMsg("The Passwords don't match!");
            response.sendRedirect("register_form.jsp");
        }
    } else {
        response.sendRedirect("register_form.jsp");
    }


%>
