package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_005fform_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<head>\n");
      out.write("    <title>TMessenger: ");
      out.print(request.getAttribute("title"));
      out.write("</title>\n");
      out.write("    <link href=\"templates/bootstrap/css/bootstrap.css\" rel=\"stylesheet\"/>\n");
      out.write("    <link href=\"templates/bootstrap/css/bootstrap-responsive.css\" rel=\"stylesheet\" />\n");
      out.write("    <link href=\"templates/bootstrap/css/signin.css\" rel=\"stylesheet\"/>\n");
      out.write("    <script type=\"text/javascript\" src=\"templates/bootstrap/js/bootstrap.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"templates/bootstrap/js/jquery-2.1.1.js\"></script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("    <div id=\"header\"><h1>TMessenger</h1></div>\n");
      out.write("    <div class='container text-center'>\n");
      out.write("        <div class ='container'>\n");
      out.write("            <script type=\"text/javascript\">\n");
      out.write("                alert(\"Hello Worlds\");\n");
      out.write("                console.log(\"Hello World\");\n");
      out.write("                </script>\n");
      out.write("            <p class=\"alert alert-error\">");
      out.print( src.Functions.ErrorMsg);
      out.write("</p>\n");
      out.write("            <form method=\"post\" action=\"login.jsp\" class=\"form-signin\" role=\"form\">\n");
      out.write("                <h2 class=\"form-signin-heading\">Please sign in</h2>\n");
      out.write("                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" required autofocus><br/>\n");
      out.write("                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\" required><br/>\n");
      out.write("                <button class=\"btn btn-primary\" type=\"submit\">Log In</button>\n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("        <div>or <a href=\"register_form.jsp\">register</a> for an account</div>\n");
      out.write("\n");
      out.write("    </div>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
