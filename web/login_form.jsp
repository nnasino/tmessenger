<%-- 
Document   : login_form
Created on : Jun 12, 2014, 8:18:23 AM
Author     : Chigozirim
--%>

<%@page import="com.torti.functions.Functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TMessenger</title>
        <link href="templates/bootstrap/css/bootstrap.css" rel="stylesheet"/>
        <link href="templates/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
        <link href="templates/bootstrap/css/signin.css" rel="stylesheet"/>
        <script type="text/javascript" src="templates/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="templates/bootstrap/js/jquery-1.8.2.min.js"></script>
        <style>
            body{
               background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <!--
        <div class="navbar">
            <div class="navbar-inner">
                <a class="brand" href="index.jsp">TMessenger</a>
                <ul class="nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="index.jsp">Profile</a></li>
                    <li><a href="index.jsp">Messages</a></li>
                </ul>
            </div>
        </div>
        -->

        <div class="container-fluid text-center">
            <br/>
            <br/><br/>
            <div style="background-color:#fdfdfd;border-radius: 10px" class ="span4 offset4 text-center">
                <div id="header"><h1 style="font-family: sans-serif">TMessenger</h1></div>
                <p id="errormessage" class="text-error"><%=Functions.ErrorMsg%></p>
                <form method="post" action="login.jsp" class="form-signin" role="form">

                    <input id ="username" type="text" class="form-control" placeholder="Username" name="username" required><br/>
                    <input type="password" class="form-control" placeholder="Password" name="password" required><br/>
                    <button class="btn btn-primary" type="submit">Log In</button>
                </form>
                <div>or <a href="register_form.jsp">register</a> for an account</div>
                <br/> <br/><br/><br/>
                <%@include file="templates/footer.jsp"%>
            </div>
                
        </div>
                
    </body>
</html>