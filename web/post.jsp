<%-- 
    Document   : post.jsp
    Created on : Jun 23, 2014, 12:10:44 PM
    Author     : Chigozirim
--%>

<%@page import="sun.misc.Regexp"%>
<%@page import="com.mysql.jdbc.CallableStatement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.torti.functions.Functions"%>
<%@page import="java.sql.SQLException"%>
        
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% if (request.getMethod().equals("POST")) {
        try {
            if (Functions.addPost(Integer.parseInt(session.getAttribute("userId").toString()),request.getParameter("posttext"),
                    Integer.parseInt(session.getAttribute("currentThreadId").toString()), Calendar.getInstance().getTime())) {

                request.getRequestDispatcher(String.format("conversation.jsp?t=%s&s=%s",
                        session.getAttribute("currentThreadId"), session.getAttribute("threadStart"))).forward(request, response);

            }
        }catch(NullPointerException exc ){
            out.println(exc.getMessage());
             out.println(session.getAttribute("userId"));
             out.println(request.getParameter("posttext"));
             out.println(session.getAttribute("currentThreadId"));
             out.println(Functions.sqlDateFormatter.format(Calendar.getInstance().getTime()));
            }
        catch(SQLException exc){
            out.println(exc.getMessage());
            out.println(session.getAttribute("userId"));
             out.println(request.getParameter("posttext"));
             out.println(session.getAttribute("currentThreadId"));
             out.println(Functions.sqlDateFormatter.format(Calendar.getInstance().getTime()));
        }
        
    } else {
        response.sendRedirect("conversations.jsp");
    }%>