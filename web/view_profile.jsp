<%-- 
    Document   : view_profile.jsp
    Created on : Jun 24, 2014, 2:23:33 PM
    Author     : Chigozirim
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.torti.functions.Functions"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="templates/header.jsp"%>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">TMessenger</a>
            <ul class="nav">
                <li><a href="index.jsp">Home</a></li>
                <li class="active"><a href="view_profile.jsp">Profile</a></li>
                <li><a href="index.jsp">Messages</a></li>
            </ul>
        </div>
    </div>

<div class="container- fluid text-center">
    <% String user = "";
        if(request.getParameter("user")==null){
        user = (String) session.getAttribute("username");
    }else{
            user = (String) request.getParameter("user");
        }
        ResultSet set = Functions.getUser(user);
        set.next();
    %>
    <p class="large">Name: <%=set.getString(4) +" " + set.getString(5)%> </p>
    <p class="large">Username: <%=set.getString(2)%></p>
    <p class="large">Email: <%=set.getString(3)%></p>
    <p class="large">Sex: <%= set.getString(6)%></p>
    <p class="large">Location: <%=set.getString(7)%></p>
</div>

<%@include file="templates/footer.jsp"%>
