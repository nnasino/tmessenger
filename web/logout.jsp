<%-- 
    Document   : logout.jsp
    Created on : Jun 15, 2014, 5:11:29 PM
    Author     : Chigozirim
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% if(session.getAttribute("username")==null){
    //request.setAttribute("ErrorMsg", "");
    com.torti.functions.Functions.setErrorMsg("");
    response.sendRedirect("index.jsp");
}else{
    session.removeAttribute("username");
    session.removeAttribute("userId");
    session.removeAttribute("threadStart");
   // request.setAttribute("ErrorMsg", "");
    com.torti.functions.Functions.setErrorMsg("");
    request.setAttribute("isLogout", "Y");
    response.sendRedirect("index.jsp");
}%>
