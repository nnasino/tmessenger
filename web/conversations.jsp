<%-- 
    Document   : conversations.jsp
    Created on : Jun 14, 2014, 8:36:51 PM
    Author     : Chigozirim
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="templates/header.jsp"%>
<%@page import="com.torti.functions.*"%>
<!--header-->
<div class="navbar">
    <div class="navbar-inner">
        <a class="brand" href="#">TMessenger</a>
        <ul class="nav">
            <li><a href="index.jsp">Home</a></li>
            <li><a href="view_profile.jsp">Profile</a></li>
            <li class="active"><a href="#">Messages</a></li>                
        </ul>
        <ul class="nav pull-right">
            <li class="pull-right"><a class="pull-right" href="logout.jsp">Log Out</a></li>
        </ul>
    </div>
</div>

<%//if the user isn't logged in redirect to the login page
    if (session.getAttribute("username") == null) {
        response.sendRedirect("login.jsp");
    }%>
<div class="container text-center">

    Welcome, <%= session.getAttribute("username")%>, Click a message to view the conversation.
    <div>
        <p class="text-info"><%=com.torti.functions.Functions.postThreadMsg%></p>
        <div class="container text-center">
            <table class="table table-striped">
                <thead>
                    <tr><td>Your Conversations</td></tr>
                </thead>
                <tbody>
                    <%
                        //Reopen the connection if it is closed
                        if (Functions.conn.isClosed()) {
                            Functions.conn = DriverManager.getConnection(Functions.CONNECTION_URL,
                                    Functions.USERNAME, Functions.PASSWORD);
                        }

                        //create the statement
                        Functions.stmt = Functions.conn.createStatement();

                        //Get the number of threads
                        ResultSet set = null;
                        set = Functions.stmt.executeQuery(String.format(Functions.noOfThreadsQuery,
                                session.getAttribute("userId").toString(), session.getAttribute("userId").toString()));
                        int threadStart = Integer.parseInt(request.getParameter("s")) * 20;
                        set.next();
                        
                        //This is the point where we begin to fetch threads
                        int count = set.getInt(1);

                        //Fetch the threads
                        String sqlStatement = String.format(Functions.fetchThreadsQuery,
                                session.getAttribute("userId").toString(), session.getAttribute("userId").toString(),
                                threadStart);
                        ResultSet threads = Functions.stmt.executeQuery(sqlStatement);

                        //render the threads
                        while (threads.next()) {%>
                    <!--render each conversation/thread-->
                    <tr><td><a href="<%="conversation.jsp?t=" + threads.getString(1) + "&s=" + 0%>"><%=threads.getString(4)%></a><br/>

                            <span class="userinfo"><%=Functions.getUserName(threads.getString(2))%></b></a> to 
                                <b><%= Functions.getUserName(threads.getString(3))%></b></span>
                                <span style="float:right"><small>Posted on: <%= threads.getDate(7)%></small></span>
                            <p></p></td></tr>
                            <% }%>

                </tbody>
            </table>
            <div class="inline">
                <div class="text-right"><a href="createmessage_form.jsp"><button class="btn">Create a new Message</button></a></div>
                <!--Pagination-->
                <div class="pagination pagination-right">
                    <ul>
                        <li class="<%= (request.getParameter("s").equals("0")) ? "disabled" : "enabled"%>">
                            <%=(threadStart == 0) ? "<a>&laquo;</a>"
                                    : "<a href=\"conversations.jsp?s=" + ((threadStart - 20) / 20) + "\">&laquo;</a>"%></li>

                        <% int i = 0;
                            while (i < count) {%>
                        <li <%=(threadStart == (i)) ? "class=\"active\"><a>" + (i / 20 + 1) + "</a>" : "><a href=\"conversations.jsp?s=" + (i / 20) + "\">" + (i / 20 + 1) + "</a>"%></li>       
                            <%       i += 20;
                                }%>
                        <li class="<%= (count - threadStart <= 20) ? "disabled" : "enabled"%>">
                            <%=(count - threadStart <= 20) ? "<a>&raquo;</a>"
                                    : "<a href=\"conversations.jsp?s=" + ((threadStart + 20) / 20) + "\">&raquo;</a>"%></li>
                    </ul>
                </div>
            </div>
        </div>
        <a href="logout.jsp">Log Out</a>
    </div>
</div>


<%@include file="templates/footer.jsp" %>
