<%-- 
    Document   : register_form.jsp
    Created on : Jun 15, 2014, 8:10:44 PM
    Author     : Chigozirim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="templates/header.jsp"%>
<br/><br/><br/>
<div class="container text-center">
    <form method="post" action="register.jsp">
        <p class="text-error"><%= com.torti.functions.Functions.registerErrorMsg%></p>
        <input type="text" placeholder="Username" name="uname" required autofocus/><br/>
        <input type="email" placeholder="Email Address" name="email" required /><br/>
        <input type="text" placeholder="First Name" name="fname" required/><br/>
        <input type="text" placeholder="Last Name" name="lname" required/><br/>
        <input type="password" placeholder="Password" name="pword1" required/><br/>
        <input type="password" placeholder="Confirm Password" name="pword2" required/><br/>
        <select name="sex">
            <option>Male</option>
            <option>Female</option>
        </select><br/>
        <input type="text" placeholder="Location" name="loc" required/><br/>
        <button type="submit" class="btn btn-primary">Register</button>
    </form>
</div>
        <%@include file="templates/footer.jsp"%>
