<%-- 
    Document   : post_form
    Created on : Jun 23, 2014, 12:10:13 PM
    Author     : Chigozirim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="templates/header.jsp" %>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">TMessenger</a>
            <ul class="nav">
                <li><a href="index.jsp">Home</a></li>
                <li><a href="view_profile.jsp">Profile</a></li>
                <li class="active"><a href="#">Messages</a></li>
            </ul>
        </div>
    </div>
<div class="container text-center">
    <p class="text-center">Create new Reply</p>
    <form method="post" action="post.jsp">
        <textarea rows = "9" cols="20" name="posttext" placeholder="Enter your text...." autofocus required></textarea><br/>
        <button type="submit" class="btn btn-primary">Create Reply</button>
    </form>
</div>
    <%@include file="templates/footer.jsp"%>