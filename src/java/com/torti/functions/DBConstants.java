package com.torti.functions;

public interface DBConstants {
    static String DRIVER = "com.mysql.jdbc.Driver";
    static String CONNECTION_URL = "jdbc:mysql://localhost/challenge";
    static String USERNAME = "root";
    static String PASSWORD = "";
}
