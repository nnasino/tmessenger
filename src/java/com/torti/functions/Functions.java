package com.torti.functions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Functions implements DBConstants {

    public static int loginResult = 0;
    
    //Date formatters
    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
    public static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mmaa");
    public static final SimpleDateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    //Error messages
    public static String ErrorMsg = "";
    public static String registerErrorMsg = "";
    public static String PostThreadErrorMsg = "";
    public static String postThreadMsg = "";
    
    //User authentication constants
    public static final int USER_VALIDATED = 1;
    public static final int USER_NOT_FOUND = 2;
    public static final int PASSWORD_INCORRECT = 3;
    
    public static Connection conn = null;
    public static Statement stmt = null;
    public static String sqlStatement = null;
    public static ResultSet resultset = null;
    
    //Query strings
    private static final String loginQuery = "SELECT `Userid`, `Password` FROM `users` WHERE `UserName` = '%s';";
    private static final String addUserQuery = "INSERT INTO `challenge`.`users` (`UserName`, `Email`, `FirstName`, `LastName`, `Password`, `Sex`, `Location`) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');";
    public static final String fetchThreadsQuery = "SELECT * FROM `threads` WHERE `AuthorId` = %s UNION SELECT * FROM `threads`"
            + " WHERE `ReceiverId` = %s ORDER BY `LastPostTime` DESC LIMIT %s, 20 ";
    public static final String noOfThreadsQuery = "SELECT COUNT(*) FROM (SELECT * FROM `threads` WHERE `AuthorId` = %s UNION SELECT * FROM `threads` WHERE `ReceiverId` = %s) AS COUNTS";
    public static final String noOfPostsQuery = "SELECT COUNT(*) FROM `posts` WHERE `ThreadId` = %s";
    public static final String fetchPostsQuery = "SELECT * FROM `posts` WHERE `ThreadId` = %s ORDER BY `TimePosted` ASC LIMIT %s, 20";

    private static final String addPostQuery = "INSERT INTO `posts` (`AuthorId`, `PostText`, `ThreadId`,"
            + " `TimePosted`) VALUES (\"%s\", \"%s\", \"%s\", \"%s\")";
    public static ResultSet threads = null;
    public static PreparedStatement addPostStmt = null;
    public static PreparedStatement addThreadStmt = null;

    static {

        try {
            Class.forName(DBConstants.DRIVER).newInstance();
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            stmt = conn.createStatement();
            addPostStmt = conn.prepareStatement("INSERT INTO `posts` (`AuthorId`, `PostText`, `ThreadId`,"
                    + " `TimePosted`) VALUES (?, ?, ?, ?)");
            addThreadStmt = conn.prepareStatement("INSERT INTO `threads` (`AuthorId`, "
                    + "`ReceiverId`, `Title`, `DateCreated`, `NewMessage`, `LastPostTime`) VALUES (?, ?, ?, ?, ?, ?)");
        } catch (ClassNotFoundException exc2) {
            exc2.printStackTrace();
        } catch (InstantiationException ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Functions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ee) {

        }

    }

    /**
     * Used to authenticate a user
     * @param username username
     * @param password password
     * @return
     * @throws SQLException 
     */
    public static int login(String username, String password) throws SQLException {
        if (conn.isClosed()) {
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
        }
        stmt = conn.createStatement();
        String pass = null;
        sqlStatement = String.format(loginQuery, username);
        resultset = stmt.executeQuery(sqlStatement);
        resultset.next();
        pass = resultset.getObject(2).toString();
        if (pass == null) {
            closeConnections();
            return USER_NOT_FOUND;
        } else if (pass.equals(hashPassword(password))) {
            // closeConnections();
            return Integer.parseInt(resultset.getObject(1).toString());
        } else {
            closeConnections();
            return PASSWORD_INCORRECT;
        }
    }

    /**
     * This is used to register a new user into the database.
     *
     * @param userName The chosen user name
     * @param email The chose email address
     * @param firstName The first name of the user
     * @param lastName The last name of the user
     * @param password The password of the user
     * @param sex The sex of the user
     * @param location The location of the user
     * @return An <code>int</code> 1 or 2. <br/> 1. If the user was successfully
     * registered <br/> 2. If there was an error
     * @throws SQLException throws a SQLException for any db related errors.
     *
     */
    public static int addUser(String userName, String email, String firstName,
            String lastName, String password, String sex, String location) throws SQLException {
        if (conn.isClosed()) {
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
        }
        stmt = conn.createStatement();
        sqlStatement = String.format(addUserQuery, userName,
                email, firstName, lastName, hashPassword(password), sex, location);
        int result = stmt.executeUpdate(sqlStatement);
        if (result > 0) {
            closeConnections();
            return 1;
        } else {
            closeConnections();
            return 2;
        }
    }
    /**
     * This creates a post
     * @param userId the id of the user
     * @param text the text body of the post
     * @param threadId the threadid of the thread
     * @param currentTime the current time
     * @return <code>true</code> if the post was successfully added and<code>false</code> otherwise.
     * @throws SQLException 
     */
    public static boolean addPost(int userId, String text, int threadId, Date currentTime) throws SQLException {
        if (conn.isClosed()) {
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            addPostStmt = conn.prepareStatement("INSERT INTO `posts` (`AuthorId`, `PostText`, `ThreadId`,"
                    + " `TimePosted`) VALUES (?, ?, ?, ?)");
        }
        addPostStmt.setInt(1, userId);
        addPostStmt.setString(2, text);
        addPostStmt.setInt(3, threadId);
        addPostStmt.setString(4, sqlDateFormatter.format(currentTime));
        addPostStmt.execute();
        //if adding the post was successful then set the last post time to the time of the new post
        if(addPostStmt.getUpdateCount() > 0){
            sqlStatement =  "UPDATE `threads` SET `LastPostTime` = '%s' WHERE threadId = '%s'";
            stmt = conn.createStatement();
            stmt.executeUpdate(String.format(sqlStatement, sqlDateFormatter.format(currentTime), threadId));
            return true;                    
        }
        else{
            return false;
        }
    }

    /**
     * This is used to create a new thread.
     * @param authorId The id of the user sending the message
     * @param receiverId The id of the user receiving the message
     * @param title The title of the message
     * @param message - The body of the first post
     * @param currentTime The current time 
     * @return <code>True</code> if the thread was successfully created and<code>false</code> otherwise.
     * @throws SQLException 
     */
    public static boolean addThread(int authorId, int receiverId, String title, String message,
            Date currentTime) throws SQLException {
        if (conn.isClosed()) {
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            addThreadStmt = conn.prepareStatement("INSERT INTO `threads` (`AuthorId`, "
                    + "`ReceiverId`, `Title`, `DateCreated`, `NewMessage`, `LastPostTime`) VALUES (?, ?, ?, ?, ?, ?)");
        }
        addThreadStmt.setInt(1, authorId);
        addThreadStmt.setInt(2, receiverId);
        addThreadStmt.setString(3, title);
        addThreadStmt.setString(4, sqlDateFormatter.format(currentTime));
        addThreadStmt.setInt(5, 0);
        addThreadStmt.setString(6, sqlDateFormatter.format(currentTime));
        addThreadStmt.execute();
        if (addThreadStmt.getUpdateCount() > 0) {
            sqlStatement = "SELECT `ThreadId` FROM `Threads` WHERE `title`= '%s' AND `LastPostTime` = '%s'";
            stmt = conn.createStatement();
           
            ResultSet set =  stmt.executeQuery(String.format(sqlStatement,title, 
                    sqlDateFormatter.format(currentTime)));
            set.next();
            
            if (addPost(authorId, message, set.getInt(1), currentTime)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This is used to retrieve the username of a given user
     *
     * @param userId The userId of the required user.
     * @return A <code>String</code> representing the username of the user
     * @throws SQLException For db related errors.
     */
    public static String getUserName(String userId) throws SQLException {
        if (conn.isClosed()) {
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
        }
        stmt = conn.createStatement();
        sqlStatement = String.format("SELECT `UserName` FROM `users` WHERE `UserId` = %s", userId);
        ResultSet result = stmt.executeQuery(sqlStatement);
        String username = null;
        if (result.next()) {
            username = result.getObject(1).toString();
            stmt.close();
            return username;
        } else {
            stmt.close();
            return null;
        }
    }
    
    
    /**Provides the userid of the user specified by the passed <code>username</code>
     * 
     * @param username The username of the user
     * @return the userid of the user or -1 if the user was not found
     * @throws SQLException If an sql error occurs
     */
    public static int getUserId(String username) throws SQLException{
        if(conn.isClosed()){
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            
        }
        stmt = conn.createStatement();
        sqlStatement = String.format("SELECT `UserId` FROM `users` WHERE `userName` = '%s'", username);
        ResultSet result = stmt.executeQuery(sqlStatement);
        int userId = -1;
        if(result.next()){
            userId = result.getInt(1);
            stmt.close();
            return userId;
        }else{
            stmt.close();
            return -1;
        }
    }
    
    /**Returns a result set containing all the info of the user identified by username.
     * 
     * @param username The username of the user
     * @return The resultSet object containing information about the user
     * @throws SQLException  if an sql error occurs
     */
    public static ResultSet getUser(String username)throws SQLException{
         if(conn.isClosed()){
            conn = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
            
        }
        stmt = conn.createStatement();
        sqlStatement = String.format("SELECT * FROM `users` WHERE `userName` = '%s'", username);
        ResultSet result = stmt.executeQuery(sqlStatement);
        return result;
    }

    /**
     * Sets the error message variable. This message is displayed on the login form
     * @param mesg  The message
     */
    public static void setErrorMsg(String mesg) {
        ErrorMsg = mesg;
    }

    /**
     * Sets the register error message variable. this message is displayed on the register form if an
     * error occurs.
     * @param registerErrorMsg The message
     */
    public static void setRegisterErrorMsg(String registerErrorMsg) {
        Functions.registerErrorMsg = registerErrorMsg;
    }

    /**Closes the connections to the database
     * 
     * @throws SQLException  if an error occurs
     */
    private static void closeConnections() throws SQLException {
        stmt.close();
        conn.close();
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * bytes to hex method culled from stack overflow.com
     * http://stackoverflow.com/a/9855338/2970947
     *
     * @param bytes The bytes to convert
     * @return the hexadecimal equivalent
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private static final String SALT = "49911994";

    /**
     * Password hashing method also culled from stackoverflow.com
     * http://stackoverflow.com/questions/20832008/jsp-simple-password-encryption-decryption
     */
    public static String hashPassword(String in) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(SALT.getBytes());
            md.update(in.getBytes());
            byte[] out = md.digest();
            return bytesToHex(out);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    //
}
